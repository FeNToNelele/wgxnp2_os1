#!/usr/bin/python3

last=[]

read = open('nd/GetNameday.txt', 'r')
last = read.readlines()
last[0] = last[0].split(sep="{")
read.close()

a = last[0][4].split(sep=":")

b=a[1].split(sep=",")
#names are splitted, but they still have:
#	-quotation marks
#	-curly brackets

quotationCleared = [None]*len(b)


if(len(b)==1):
    quotationCleared[0] = b[0][1:-4]

if(len(b) == 2):
    quotationCleared[0] = b[0][1:]
    format=b[1]
    quotationCleared[1] = format[:-4].strip('"')

j=0
if(len(b) > 2):
    for i in b:
        if(j==0):
            quotationCleared[j]=i[1:]
        elif(j == len(b)-1):
            quotationCleared[j]=i[1:-4].strip('"')
        else:
            quotationCleared[j]=i[1:]
        j = j+1

#counter=0
#for name in b:
#  if(counter==len(b)-1):
#    quotationCleared[counter]=name[1:-4]
#
#  if (len(b) != 1):
#    quotationCleared[counter]=name[1:-1]

#  counter = counter+1

#Names only have unicode characters from now on.

def unicodeChar(input):
    switcher = {
    '\\u00e1': "á",
    '\\u00e9': "é",
	'\\u00ed': "í",
	'\\u00f3': "ó",
	'\\u0151': "ő",
	'\\u00f6': "ö",
	'\\u00fa': "ú",
	'\\u0171': "ű",
	'\\u00fc': "ü"
    }
    return switcher.get(input, "Invalid unicode value.")


#id (what if a name contains more than one unicode value)
#startIndex
#endIndex

result = list()

for a in range(len(quotationCleared)):
  j=0 #for characters
  foundUnicode = False;
  for i in quotationCleared[a]:
    if (i=="\\"):
      foundUnicode = True;
      endIndex=j+6
      toFormat = quotationCleared[a][j:endIndex]
      formatted=quotationCleared[a][0:j] + unicodeChar(toFormat) + \
                             quotationCleared[a][endIndex:]
      quotationCleared[a]=formatted
      j = j-5;
      #print(quotationCleared[a])
      #result.append(formatted)
    j = j+1
  if(j == len(quotationCleared[a])):
    result.append(quotationCleared[a])

  #if(foundUnicode == False): #Name did not contain unicode characters, so we're fine
  #  result.append(quotationCleared[a])

counter = 0
for i in result:
  print(result[counter])
  counter = counter + 1
