#!/usr/bin/python3

print("Hogy hívják, akit szeretnél hozzáadni?")
name = input()

print("Melyik hónapban született? Hónap száma:")
month = input()

print("Melyik napon született?")
day = input()

class e(Exception):
    pass

class InvalidValueException(e):
    pass

class NumTooHighException(e):
    pass

try :
    if(int(month) < 1):
        raise InvalidValueException
    if(int(month) > 12):
        raise InvalidValueException
except InvalidValueException:
    print("Ilyen számú hónap nem létezik!")
except:
    print("Hibás bemeneti formátum! (A hónap számát kell megadni!)")

try:                                #What if 30th of February?
    if(int(day) < 1):
        raise InvalidValueException
    if(int(day) > 31):
        raise InvalidValueException
except InvalidValueException:
    print("Nincs ilyen nap a naptárban.")
except:
    print("Hibás bemeneti formátum a napnál!")

if (int(month) < 10 and month[0]!='0'):
    month="0"+month;

if (int(day) < 10):
    day="0"+day;

toWrite= month+day+";"+name

#print(toWrite)

try:
  f = open("./bd/bdays.txt","a")
  f.write(toWrite)
  f.write("\n")
  f.close();
  print("Sikeres mentés!")
except:
  print("Hiba, nem sikerült elmenteni.")
