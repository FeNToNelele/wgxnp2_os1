!#/bin/bash

curl -X 'GET' \
  'https://api.abalin.net/today?country=hu&timezone=Europe%2FPrague' \
  -H 'accept: application/json' | iconv -t utf-8 > nd/GetNameday.txt

clear

echo "Szia $USER!"

while getopts ":hnba" opt; do
  case ${opt} in
    h ) echo "A -n megmutatja, hogy kinek van ma névnapja."
	echo "A -b megmutatja, hogy melyik ismerősödnek van születésnapja."
	echo "A -a segítségével megadhatsz új születésnapokat."
      ;;
    n ) echo "A mai névnapos(ok): "
	./scrape.py
      ;;
    b )	./GetBday.py
      ;;
    a )	./AddBday.py
      ;;
    \? ) echo "Hibás kapcsolóhasználat. Segítség: -h"
      ;;
  esac
done
