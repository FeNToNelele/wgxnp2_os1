#!/usr/bin/python3

read = open('./bd/bdays.txt', 'r')
row = read.readlines()

read.close()

dates = [None]*len(row)
name = [None]*len(row)

i = 0
while(i < len(dates)):
    dates[i] = row[i][0:4]
    name[i] = row[i][5:]
    i = i + 1

from datetime import date

todayObj = date.today()
monthday=todayObj.strftime("%m%d")

result = list()
counter = 0

for i in dates:
    if(monthday == i):
        result.append(name[counter])
    counter = counter + 1


if(len(result) == 0):
    print("Ma egy ismerősödnek sincs születésnapja.")
else:
    print("Ma az alábbi ismerőseidnek van születésnapja:")
    for i in result:
        print(i)



