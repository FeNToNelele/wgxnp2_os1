____________________________EN____________________________

1. Introduction
This project was made for a university assignment.

2. Features
The script can:
-     show who celebrates his/her nameday
-     check which friends of yours celebrate their birthday
-     add new birthdays and remind you about that.


3. How to use?

0: Unzip.

- [ ] Open Terminal.
- [ ] Cd into the directory.
- [ ] ./script.sh

Options:
-         -n : Get current nameday.
-         -b : Get birthdays.
-         -a : Add birthday.

You get help via -h.


4. Note
I use InternationalNamedayAPI to get the actual nameday.
I visit a Hungarian university, so the script is also in that language.

InternationalNamedayAPI: https://app.swaggerhub.com/apis/nekvapil/InternationalNamedayAPI/3.0.0#/default/get_today





____________________________HU____________________________

1. Bevezetés
Ez a projekt egy egyetemi tárgyam beadandója.

2. Mit tud?
A script képes:
-     megmutatni, hogy ki ünnepli a névnapját
-     megnézni, hogy melyik ismerősöd ünnepli a születésnapját
-     hozzáadni új születésnapokat és azokról értesíteni.

3. Hogyan használjam?

0: Bontsd ki a fájlt.

- [ ] Nyisd meg a Terminált.
- [ ] Cd-zz a projekt könyvtárába.
- [ ] ./script.sh


Kapcsolók:
-         -n : Adott névnap.
-         -b : Aznapi szülinaposok.
-         -a : Új születésnap hozzáadása.

A -h segítséget jelenít meg.

4. Megjegyzés
A névnapi adatok letöltésére az InternationalNamedayAPI-t használom.
Mivel magyar egyetemre járok, ezért a script is ilyen nyelvű.

InternationalNamedayAPI: https://app.swaggerhub.com/apis/nekvapil/InternationalNamedayAPI/3.0.0#/default/get_today
